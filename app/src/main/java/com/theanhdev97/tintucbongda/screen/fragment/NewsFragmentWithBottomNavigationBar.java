package com.theanhdev97.tintucbongda.screen.fragment;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatDelegate;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.theanhdev97.tintucbongda.R;
import com.theanhdev97.tintucbongda.screen.adapter.MyFragmentPagerAdapter;
import com.theanhdev97.tintucbongda.screen.data.model.general.ListFragmentData;
import com.theanhdev97.tintucbongda.screen.util.Const;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by DELL on 16/05/2018.
 */

public class NewsFragmentWithBottomNavigationBar extends Fragment {
  @BindView(R.id.view_pager)
  ViewPager mViewPager;
  @BindView(R.id.tabs)
  TabLayout mTabLayout;

  private Context mContext;
  private MyFragmentPagerAdapter mPagerAdapter;

  @Nullable
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    mContext = getContext();
    return inflater.inflate(R.layout.fragment_news_with_bottom_navigation_bar, container, false);
  }

  @TargetApi(Build.VERSION_CODES.LOLLIPOP)
  @Override
  public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    ButterKnife.bind(this, view);
    AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    prepareViewPager();
  }

  void prepareViewPager() {
    mPagerAdapter = new MyFragmentPagerAdapter(getChildFragmentManager());

    ListFragmentData data = (ListFragmentData) getArguments().getSerializable(Const.KEY_LIST_FRAGMENTS_DATA);
    for (Pair<String, String> item : data.list) {
      DetailNewsFragment fragment = new DetailNewsFragment();
      Bundle bundle = new Bundle();
      bundle.putString(Const.KEY_NEWS_URL, item.second);
      fragment.setArguments(bundle);
      mPagerAdapter.addFragment(fragment, item.first);
    }

    mViewPager.setAdapter(mPagerAdapter);
    mTabLayout.setupWithViewPager(mViewPager);
    if (data.list.size() > 3) {
      mTabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
    } else
      mTabLayout.setTabMode(TabLayout.MODE_FIXED);
    mTabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);

  }
}
