package com.theanhdev97.tintucbongda.screen.screen;

import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.theanhdev97.tintucbongda.R;
import com.theanhdev97.tintucbongda.screen.fragment.NewsFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Home2Activity extends AppCompatActivity {
  @BindView(R.id.bottom_navigation)
  BottomNavigationView mBottomNavigationView;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_home2);
    ButterKnife.bind(this);
    repairUI();
    setEventListener();
  }

  void setEventListener() {
    mBottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
      @Override
      public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
          case R.id.action_news:
            Toast.makeText(Home2Activity.this, "news", Toast.LENGTH_SHORT).show();
            break;
          case R.id.action_about:
            Toast.makeText(Home2Activity.this, "about", Toast.LENGTH_SHORT).show();
            break;
          default:
            break;
        }
        return true;
      }
    });
  }

  void repairUI() {
    NewsFragment newsFragment = new NewsFragment();
    addFragment(newsFragment);
  }

  void addFragment(Fragment fragment) {
    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
    transaction.add(R.id.frame_main, fragment);
    transaction.commit();
  }

  void replaceFragment(Fragment fragment) {
    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
    transaction.replace(R.id.frame_main, fragment);
    transaction.addToBackStack(null);
    transaction.commit();
  }
}
