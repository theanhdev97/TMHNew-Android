package com.theanhdev97.tintucbongda.screen.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.theanhdev97.tintucbongda.R;
import com.theanhdev97.tintucbongda.screen.adapter.MyFragmentPagerAdapter;
import com.theanhdev97.tintucbongda.screen.data.model.eventbus.TabPosition;
import com.theanhdev97.tintucbongda.screen.screen.CategoryActivity;
import com.theanhdev97.tintucbongda.screen.screen.HomeActivity;
import com.theanhdev97.tintucbongda.screen.util.Const;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by DELL on 21/05/2018.
 */

public class NewsFragment extends Fragment {
  @BindView(R.id.view_pager)
  ViewPager mViewPager;
  @BindView(R.id.tabs)
  TabLayout mTabLayout;
  @BindView(R.id.imv_category)
  ImageView mImageViewCategory;
  @BindView(R.id.layout_category)
  RelativeLayout mRelativeLayoutCategory;

  MyFragmentPagerAdapter mPagerAdapter;
  String[] mListTitle;
  String[] mListUrls;
  Context mContext;

  @Nullable
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    mContext = getContext();
    return inflater.inflate(R.layout.fragment_news, container, false);
  }

  @Override
  public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    ButterKnife.bind(this, view);
    EventBus.getDefault().register(this);
    initData();
    prepareUI();
  }

  @Subscribe(threadMode = ThreadMode.MAIN)
  public void callbackToSetTabPosition(TabPosition tabPosition) {
    mViewPager.setCurrentItem(tabPosition.position);
  }

  void initData() {
    mListTitle = getResources().getStringArray(R.array.news_title);
    mListUrls = getResources().getStringArray(R.array.news_url);
  }

  void addFragmentToViewPager() {
    for (int i = 0; i < mListTitle.length; i++) {
      String title = mListTitle[i];
      String url = mListUrls[i];
      DetailNewsFragment fragment = new DetailNewsFragment();
      Bundle bundle = new Bundle();
      bundle.putString(Const.KEY_NEWS_URL, url);
      fragment.setArguments(bundle);
      mPagerAdapter.addFragment(fragment, title);
    }
  }

  void prepareUI() {
    mRelativeLayoutCategory.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Intent i = new Intent(mContext, CategoryActivity.class);
        startActivity(i);
      }
    });

    // init tab + viewpager
    mPagerAdapter = new MyFragmentPagerAdapter(getFragmentManager());
    addFragmentToViewPager();
    mViewPager.setAdapter(mPagerAdapter);
    mTabLayout.setupWithViewPager(mViewPager);
  }
}
