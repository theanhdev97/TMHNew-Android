package com.theanhdev97.tintucbongda.screen.screen;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.theanhdev97.tintucbongda.R;
import com.theanhdev97.tintucbongda.screen.adapter.CategoryAdapter;
import com.theanhdev97.tintucbongda.screen.adapter.NewsClickListener;
import com.theanhdev97.tintucbongda.screen.data.model.eventbus.TabPosition;
import com.theanhdev97.tintucbongda.screen.data.model.general.Category;
import com.theanhdev97.tintucbongda.screen.util.L;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CategoryActivity extends AppCompatActivity implements NewsClickListener {
  @BindView(R.id.rclv_category)

  RecyclerView mRecyclerViewCategory;
  ArrayList<Category> mListCategories;
  CategoryAdapter mCategoryAdapter;
  String[] mListTitles;
  int[] mListImageIDs;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_catagory);
    ButterKnife.bind(this);
    initData();
    prepareUI();
  }

  void initData() {
    mListCategories = new ArrayList<Category>();
    initArray();
    for (int i = 0; i < mListTitles.length; i++) {
      String title = mListTitles[i];
      int imageID = mListImageIDs[i];
      L.d("Image : " + imageID);
      mListCategories.add(new Category(title, imageID));
    }
  }

  void initArray(){
    mListTitles = getResources().getStringArray(R.array.news_title);
    mListImageIDs = new int[mListTitles.length];
    mListImageIDs[0] = R.drawable.lastest;
    mListImageIDs[1] = R.drawable.market;
    mListImageIDs[2] = R.drawable.altcoin;
    mListImageIDs[3] = R.drawable.bitcoin;
    mListImageIDs[4] = R.drawable.blockchain;
    mListImageIDs[5] = R.drawable.trader;
    mListImageIDs[6] = R.drawable.analysis_technology;
    mListImageIDs[7] = R.drawable.analysis_market;
    mListImageIDs[8] = R.drawable.ico;
    mListImageIDs[9] = R.drawable.guide;
  }

  void prepareUI() {
    mCategoryAdapter = new CategoryAdapter(this, mListCategories);
    mRecyclerViewCategory.setLayoutManager(new GridLayoutManager(this, 2));
    mRecyclerViewCategory.setAdapter(mCategoryAdapter);
    mCategoryAdapter.setOnClickListener(this);
  }

  @Override
  public void onClick(int position) {
    EventBus.getDefault().post(new TabPosition(position));
    finish();
  }
}
