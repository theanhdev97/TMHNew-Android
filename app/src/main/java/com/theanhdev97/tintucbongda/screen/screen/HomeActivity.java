package com.theanhdev97.tintucbongda.screen.screen;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.theanhdev97.tintucbongda.R;
import com.theanhdev97.tintucbongda.screen.adapter.MyFragmentPagerAdapter;
import com.theanhdev97.tintucbongda.screen.data.model.eventbus.TabPosition;
import com.theanhdev97.tintucbongda.screen.fragment.AboutDialogFragment;
import com.theanhdev97.tintucbongda.screen.fragment.DetailNewsFragment;
import com.theanhdev97.tintucbongda.screen.util.Const;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeActivity extends AppCompatActivity {
  @BindView(R.id.toolbar)
  Toolbar mToolbar;
  @BindView(R.id.view_pager)
  ViewPager mViewPager;
  @BindView(R.id.tabs)
  TabLayout mTabLayout;
  @BindView(R.id.text_view_toolbar_title)
  TextView mTextViewToolbarTitle;
  @BindView(R.id.imv_category)
  ImageView mImageViewCategory;
  @BindView(R.id.layout_category)
  RelativeLayout mRelativeLayoutCategory;

  MyFragmentPagerAdapter mPagerAdapter;
  String[] mListTitle;
  String[] mListUrls;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    EventBus.getDefault().register(this);
    setContentView(R.layout.activity_home);
    ButterKnife.bind(this);
    initData();
    prepareUI();
    getWindow().getAttributes().windowAnimations = R.style.fade;
  }

  @Subscribe(threadMode = ThreadMode.MAIN)
  public void callbackToSetTabPosition(TabPosition tabPosition) {
    mViewPager.setCurrentItem(tabPosition.position);
  }

  void initData() {
    mListTitle = getResources().getStringArray(R.array.news_title);
    mListUrls = getResources().getStringArray(R.array.news_url);
  }

  void addFragmentToViewPager() {
    for (int i = 0; i < mListTitle.length; i++) {
      String title = mListTitle[i];
      String url = mListUrls[i];
      DetailNewsFragment fragment = new DetailNewsFragment();
      Bundle bundle = new Bundle();
      bundle.putString(Const.KEY_NEWS_URL, url);
      fragment.setArguments(bundle);
      mPagerAdapter.addFragment(fragment, title);
    }
  }

  void prepareUI() {
    setSupportActionBar(mToolbar);
    mTextViewToolbarTitle.setText("TMH");
    mRelativeLayoutCategory.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Intent i = new Intent(HomeActivity.this, CategoryActivity.class);
        startActivity(i);
      }
    });

    // init tab + viewpager
    mPagerAdapter = new MyFragmentPagerAdapter(getSupportFragmentManager());
    addFragmentToViewPager();
    mViewPager.setAdapter(mPagerAdapter);
    mTabLayout.setupWithViewPager(mViewPager);
  }
}
