package com.theanhdev97.tintucbongda.screen.data.model.general;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Pair;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by DELL on 17/05/2018.
 */

public class ListFragmentData implements Serializable,Parcelable{
  public ArrayList<Pair<String,String>> list;

  public ListFragmentData() {
    list = new ArrayList<Pair<String, String>>();
  }

  protected ListFragmentData(Parcel in) {
  }

  public static final Creator<ListFragmentData> CREATOR = new Creator<ListFragmentData>() {
    @Override
    public ListFragmentData createFromParcel(Parcel in) {
      return new ListFragmentData(in);
    }

    @Override
    public ListFragmentData[] newArray(int size) {
      return new ListFragmentData[size];
    }
  };

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel parcel, int i) {
  }
}
