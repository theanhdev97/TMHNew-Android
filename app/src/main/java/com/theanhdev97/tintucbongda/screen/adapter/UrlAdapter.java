package com.theanhdev97.tintucbongda.screen.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.theanhdev97.tintucbongda.R;
import com.theanhdev97.tintucbongda.screen.data.model.network.News;
import com.theanhdev97.tintucbongda.screen.util.ImageHelper;

import java.util.ArrayList;

/**
 * Created by DELL on 18/05/2018.
 */

public class UrlAdapter extends RecyclerView.Adapter<UrlAdapter.UrlViewHolder> {

  private Context mContext;
  private ArrayList<News> mObjects;
  private NewsClickListener mClickListener;

  public UrlAdapter(Context context, ArrayList<News> objects) {
    mContext = context;
    mObjects = objects;
  }


  @Override
  public int getItemCount() {
    if (mObjects.size() == 0)
      return 1;
    return mObjects.size();
  }


  @NonNull
  @Override
  public UrlViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    return new UrlViewHolder(LayoutInflater.from(mContext).inflate(R.layout.url_item, parent, false));
  }

  @Override
  public void onBindViewHolder(@NonNull UrlViewHolder holder, int position) {
    News news = mObjects.get(position);
    holder.btnUrl.setText(news.title);
  }


  public class UrlViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public Button btnUrl;

    public UrlViewHolder(View itemView) {
      super(itemView);
      btnUrl = itemView.findViewById(R.id.btn_url);
      btnUrl.setOnClickListener(this);
      btnUrl.setPaintFlags(btnUrl.getPaintFlags() |   Paint.UNDERLINE_TEXT_FLAG);
    }

    @Override
    public void onClick(View view) {
      mClickListener.onClick(getAdapterPosition());
    }
  }

  public void setOnClickListener(NewsClickListener listener) {
    this.mClickListener = listener;
  }
}
