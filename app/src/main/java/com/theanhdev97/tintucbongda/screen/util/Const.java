package com.theanhdev97.tintucbongda.screen.util;

import android.util.Pair;

import java.util.Map;

/**
 * Created by DELL on 29/04/2018.
 */

public class Const {
  public static final String KEY_NEWS_URL = "key_news_url";
  public static final String KEY_LIST_FRAGMENTS_DATA = "key_list_fragments_data";
  public static final String TAG = "tag123";

  public static final Pair<String, String> URL_MOI_NHAT = new Pair<String, String>("MỚI NHẤT",
      "http://tienmahoa.net.vn/wp-json/wp/v2/posts?_embed");

  public static final Pair<String, String> URL_THI_TRUONG = new Pair<String, String>("THỊ TRƯỜNG",
      "http://tienmahoa.net.vn/wp-json/wp/v2/posts?categories=14&_embed");

  public static final Pair<String, String> URL_THI_TRUONG_ALTCOIN = new Pair<String, String>
      ("ALTCOIN",
          "http://tienmahoa.net.vn/wp-json/wp/v2/posts?categories=22&_embed");

  public static final Pair<String, String> URL_THI_TRUONG_BITCOIN = new Pair<String, String>
      ("BITCOIN",
          "http://tienmahoa.net.vn/wp-json/wp/v2/posts?categories=21&_embed");

  public static final Pair<String, String> URL_THI_TRUONG_BLOCKCHAIN = new Pair<String, String>
      ("BLOCKCHAIN",
          "http://tienmahoa.net.vn/wp-json/wp/v2/posts?categories=20&_embed");
  public static final Pair<String, String> URL_KIEN_THUC_TRADER = new Pair<String, String>("KIẾN " +
      "THỨC TRADER",
      "http://tienmahoa.net.vn/wp-json/wp/v2/posts?categories=19&_embed");

  public static final Pair<String, String> URL_KIEN_THUC_TRADER_PHAN_TICH_THI_TRUONG = new
      Pair<String,
      String>
      ("PHÂN TÍCH THỊ TRƯỜNG",
      "http://tienmahoa.net.vn/wp-json/wp/v2/posts?categories=23&_embed");

  public static final Pair<String, String> URL_KIEN_THUC_TRADER_PHAN_TICH_KI_THUAT = new
      Pair<String,
          String>
      ("PHÂN TÍCH KĨ THUẬT",
          "http://tienmahoa.net.vn/wp-json/wp/v2/posts?categories=10&_embed");


  public static final Pair<String, String> URL_ICO = new Pair<String, String>("ICO",
      "http://tienmahoa.net.vn/wp-json/wp/v2/posts?categories=24&_embed");

  public static final Pair<String, String> URL_HUONG_DAN = new Pair<String, String>("HƯỚNG DẪN",
      "http://tienmahoa.net.vn/wp-json/wp/v2/posts?categories=25&_embed");
}
