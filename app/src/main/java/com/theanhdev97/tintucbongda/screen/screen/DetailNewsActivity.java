package com.theanhdev97.tintucbongda.screen.screen;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.robertsimoes.shareable.Shareable;
import com.theanhdev97.tintucbongda.R;
import com.theanhdev97.tintucbongda.screen.adapter.NewsClickListener;
import com.theanhdev97.tintucbongda.screen.adapter.UrlAdapter;
import com.theanhdev97.tintucbongda.screen.asynctask.GetNewsAsynctask;
import com.theanhdev97.tintucbongda.screen.data.model.ResponseListener;
import com.theanhdev97.tintucbongda.screen.data.model.network.News;
import com.theanhdev97.tintucbongda.screen.util.Const;
import com.theanhdev97.tintucbongda.screen.util.ImageHelper;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailNewsActivity extends AppCompatActivity implements SwipeRefreshLayout
    .OnRefreshListener, ResponseListener {
  @BindView(R.id.webview)
  WebView mWebView;
  @BindView(R.id.toolbar)
  Toolbar mToolbar;
  @BindView(R.id.swipe_refresh_layout)
  SwipeRefreshLayout mSwipeRefreshLayout;
  @BindView(R.id.imv_image)
  ImageView mImageView;

  @BindView(R.id.btnFacebook)
  ImageView btnFacebook;
  @BindView(R.id.btnGoogle)
  ImageView btnGoogle;
  @BindView(R.id.btnTwitter)
  ImageView btnTwitter;
  @BindView(R.id.layout_extra_news)
  LinearLayout mLayoutExtraNews;

  @BindView(R.id.rclv_url_category)
  RecyclerView mRecyclerViewUrlCategory;
  @BindView(R.id.rclv_url_lastest)
  RecyclerView mRecyclerViewUrlLastest;
  @BindView(R.id.scrollview)
  ScrollView mScrollView;

  private ArrayList<News> mListCategory;
  private UrlAdapter mUrlAdapterCategory;
  private ArrayList<News> mListLastest;
  private UrlAdapter mUrlAdapterLastest;
  private String mLink;
  private News mNews;
  private int getData = 0; // number to recognize get data
  // 1 - get lastest
  // 2 - get category

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_detail_news);
    ButterKnife.bind(this);
    mNews = (News) getIntent().getBundleExtra("bundle").getSerializable("content");
    prepairUI();
    setEventListener();
    configWebView(mLink);
    getListLastest();
  }

  private void getListLastest() {
    mListLastest = new ArrayList<News>();
    mListCategory = new ArrayList<News>();
    new GetNewsAsynctask(this).execute(Const.URL_MOI_NHAT.second + "&per_page=5");
  }

  private void setEventListener() {
    btnFacebook.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Shareable imageShare = new Shareable.Builder(DetailNewsActivity.this)
            .message("Share")
            .url(mNews.link)
            .socialChannel(Shareable.Builder.FACEBOOK)
            .build();
        imageShare.share();
      }
    });

    btnGoogle.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Shareable imageShare = new Shareable.Builder(DetailNewsActivity.this)
            .message("Share")
            .url(mNews.link)
            .socialChannel(Shareable.Builder.GOOGLE_PLUS)
            .build();
        imageShare.share();
      }
    });

    btnTwitter.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Shareable imageShare = new Shareable.Builder(DetailNewsActivity.this)
            .message("Share")
            .url(mNews.link)
            .socialChannel(Shareable.Builder.TWITTER)
            .build();
        imageShare.share();
      }
    });


  }

  private void reloadNews(News news){
    mNews = news;
    mWebView.loadData(mNews.content, "text/html", "UTF-8");
    mLayoutExtraNews.setVisibility(View.GONE);
    mSwipeRefreshLayout.setEnabled(false);
    mSwipeRefreshLayout.setRefreshing(false);
    getListLastest();
    mScrollView.scrollTo(0, 0);
  }

  @TargetApi(Build.VERSION_CODES.M)
  private void prepairUI() {
    setSupportActionBar(mToolbar);
    getSupportActionBar().setDisplayShowTitleEnabled(true);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        finish();
      }
    });

    SpannableString spannablecontent = new SpannableString("News");
    spannablecontent.setSpan(new ForegroundColorSpan(Color.parseColor("#ff6a00")),
        0, 4, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

    getSupportActionBar().setTitle(spannablecontent);

    mSwipeRefreshLayout.setOnRefreshListener(this);
    mSwipeRefreshLayout.setEnabled(false);

    ImageHelper.loadImage(this,
        mImageView,
        mNews.image,
        R.drawable.ic_news_placeholder);

  }

  private void configWebView(String url) {
    mWebView.setWebViewClient(new WebViewClient() {
      @Override
      public void onPageStarted(WebView view, String url, Bitmap favicon) {
        super.onPageStarted(view, url, favicon);
        mSwipeRefreshLayout.setRefreshing(true);
      }

      @Override
      public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
        super.onReceivedError(view, request, error);
        mSwipeRefreshLayout.setRefreshing(false);
        mSwipeRefreshLayout.setEnabled(false);
      }

      @Override
      public void onPageFinished(WebView view, String url) {
        super.onPageFinished(view, url);
        mSwipeRefreshLayout.setRefreshing(false);
        mSwipeRefreshLayout.setEnabled(false);
      }
    });
    mWebView.getSettings().setLoadsImagesAutomatically(true);
    mWebView.getSettings().setJavaScriptEnabled(true);
    mWebView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
    mWebView.loadData(mNews.content, "text/html", "UTF-8");
    new Handler().postDelayed(new Runnable() {
      @SuppressLint("JavascriptInterface")
      @Override
      public void run() {
        mWebView.addJavascriptInterface(this, "News");
      }
    }, 1000);
  }

  @JavascriptInterface
  public void resize(final float height) {
    DetailNewsActivity.this.runOnUiThread(new Runnable() {
      @Override
      public void run() {
        mWebView.setLayoutParams(new LinearLayout.LayoutParams(getResources().getDisplayMetrics()
            .widthPixels, (int) (height * getResources().getDisplayMetrics().density)));
      }
    });
  }

  @Override
  public void onRefresh() {
    mWebView.loadUrl(mLink);
  }

  @Override
  public void onSuccess(ArrayList<News> news) {
    getData++;
    if (getData % 2 == 1) {
      mListLastest.clear();
      mListLastest.addAll(news);

      mUrlAdapterLastest = new UrlAdapter(this, mListLastest);
      mRecyclerViewUrlLastest.setAdapter(mUrlAdapterLastest);
      mRecyclerViewUrlLastest.setLayoutManager(new LinearLayoutManager(this));
      mUrlAdapterLastest.setOnClickListener(new NewsClickListener() {
        @Override
        public void onClick(int position) {
          News news = mListLastest.get(position);
          mSwipeRefreshLayout.setEnabled(true);
          mSwipeRefreshLayout.setRefreshing(true);
          reloadNews(news);
        }
      });


      String category = mNews.category;
      new GetNewsAsynctask(this).execute("http://tienmahoa.net" +
          ".vn/wp-json/wp/v2/posts?categories=" + category + "&per_page=5");
    } else {
      mListCategory.clear();
      mListCategory.addAll(news);

      mUrlAdapterCategory = new UrlAdapter(this, mListCategory);

      mUrlAdapterCategory.setOnClickListener(new NewsClickListener() {
        @Override
        public void onClick(int position) {
          News news = mListCategory.get(position);
          mSwipeRefreshLayout.setEnabled(true);
          mSwipeRefreshLayout.setRefreshing(true);
          reloadNews(news);
        }
      });      mRecyclerViewUrlCategory.setAdapter(mUrlAdapterCategory);
      mRecyclerViewUrlCategory.setLayoutManager(new LinearLayoutManager(this));

      mLayoutExtraNews.setVisibility(View.VISIBLE);
    }
  }

  @Override
  public void onNodata() {
    mLayoutExtraNews.setVisibility(View.GONE);
  }

  @Override
  public void onFailure(String error) {
    mLayoutExtraNews.setVisibility(View.GONE);
  }
}
