package com.theanhdev97.tintucbongda.screen.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.theanhdev97.tintucbongda.R;
import com.theanhdev97.tintucbongda.screen.data.model.general.Category;
import com.theanhdev97.tintucbongda.screen.data.model.network.News;

import java.util.ArrayList;

/**
 * Created by DELL on 18/05/2018.
 */

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder> {

  private Context mContext;
  private ArrayList<Category> mObjects;
  private NewsClickListener mClickListener;

  public CategoryAdapter(Context context, ArrayList<Category> objects) {
    mContext = context;
    mObjects = objects;
  }


  @Override
  public int getItemCount() {
    return mObjects.size();
  }


  @NonNull
  @Override
  public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    return new CategoryViewHolder(LayoutInflater.from(mContext).inflate(R.layout.category_item, parent,
        false));
  }

  @Override
  public void onBindViewHolder(@NonNull CategoryViewHolder holder, int position) {
    Category category = mObjects.get(position);
    holder.tvTitle.setText(category.title);
    holder.imvIcon.setImageResource(category.image);
  }


  public class CategoryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public ImageView imvIcon;
    public TextView tvTitle;

    public CategoryViewHolder(View itemView) {
      super(itemView);
      tvTitle = itemView.findViewById(R.id.tv_title);
      imvIcon = itemView.findViewById(R.id.imv_icon);
      itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
      mClickListener.onClick(getAdapterPosition());
    }
  }

  public void setOnClickListener(NewsClickListener listener) {
    this.mClickListener = listener;
  }
}
