package com.theanhdev97.tintucbongda.screen.data.model.network;

import java.io.Serializable;

/**
 * Created by DELL on 29/04/2018.
 */

public class News implements Serializable {
//  @SerializedName("title")
//  public Title title;
//  @SerializedName("link")
//  public String link;
//  @SerializedName("content")
//  public String image;
//  @SerializedName("date")
//  public String pubDate;
//  @SerializedName("excerpt")
//  public ShortDescription shortDescription;
//
//  public News(Title title, String link, String image, String pubDate, ShortDescription shortDescription) {
//    this.title = title;
//    this.link = link;
//    this.image = image;
//    this.pubDate = pubDate;
//    this.shortDescription = shortDescription;
//  }
  public String title;
  public String link;
  public String image;
  public String pubDate;
  public String shortDescription;
  public String content;
  public String urlCategory;
  public String ulrPostTag;
  public String category;

  public News(String title, String link, String image, String pubDate, String shortDescription, String content, String urlCategory, String ulrPostTag, String category) {
    this.title = title;
    this.link = link;
    this.image = image;
    this.pubDate = pubDate;
    this.shortDescription = shortDescription;
    this.content = content;
    this.urlCategory = urlCategory;
    this.ulrPostTag = ulrPostTag;
    this.category = category;
  }

  public News(String title, String link, String image, String pubDate, String shortDescription, String content, String urlCategory, String ulrPostTag) {
    this.title = title;
    this.link = link;
    this.image = image;
    this.pubDate = pubDate;
    this.shortDescription = shortDescription;
    this.content = content;
    this.urlCategory = urlCategory;
    this.ulrPostTag = ulrPostTag;
  }

  public News(String title, String link, String image, String pubDate, String shortDescription, String content) {
    this.title = title;
    this.link = link;
    this.image = image;
    this.pubDate = pubDate;
    this.shortDescription = shortDescription;
    this.content = content;
  }

  public News(String title, String link, String image, String pubDate, String shortDescription) {
    this.title = title;
    this.link = link;
    this.image = image;
    this.pubDate = pubDate;
    this.shortDescription = shortDescription;
  }
}
