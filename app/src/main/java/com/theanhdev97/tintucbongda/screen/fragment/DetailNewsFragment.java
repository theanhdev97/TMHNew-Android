package com.theanhdev97.tintucbongda.screen.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.theanhdev97.tintucbongda.R;
import com.theanhdev97.tintucbongda.screen.adapter.NewsAdapter;
import com.theanhdev97.tintucbongda.screen.adapter.NewsClickListener;
import com.theanhdev97.tintucbongda.screen.asynctask.GetNewsAsynctask;
import com.theanhdev97.tintucbongda.screen.data.model.network.News;
import com.theanhdev97.tintucbongda.screen.data.model.ResponseListener;
import com.theanhdev97.tintucbongda.screen.screen.DetailNewsActivity;
import com.theanhdev97.tintucbongda.screen.util.Const;
import com.theanhdev97.tintucbongda.screen.util.EndlessRecyclerViewScrollListener;
import com.theanhdev97.tintucbongda.screen.util.L;
import com.theanhdev97.tintucbongda.screen.util.NetworkHelper;
import com.theanhdev97.tintucbongda.screen.util.UrlHelper;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by DELL on 29/04/2018.
 */

public class
DetailNewsFragment extends Fragment implements ResponseListener, NewsClickListener, SwipeRefreshLayout
    .OnRefreshListener {
  @BindView(R.id.recycler_view)
  RecyclerView mRecyclerView;
  @BindView(R.id.swipe_refresh_layout)
  SwipeRefreshLayout mSwipeRefreshLayout;
  @BindView(R.id.pgb_loadmore)
  ProgressBar mProgressBarLoadmore;

  private Context mContext;
  private String mUrl;
  private int mCurrentPage = 1;
  private boolean mIsFirstLaunch;
  private EndlessRecyclerViewScrollListener mEndlessRecyclerViewScrollListener;
  private NewsAdapter mNewsAdapter;
  private ArrayList<News> mNews;
  private LinearLayoutManager mLayoutManager;
//  private GridLayoutManager mLayoutManager;

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    mUrl = getArguments().getString(Const.KEY_NEWS_URL);
    mContext = getContext();
    return inflater.inflate(R.layout.detail_news_fragment, container, false);
  }

  @Override
  public void onResume() {
    super.onResume();
//    if (mIsFirstLaunch == false) {
    mCurrentPage = 1;
    getNewsByPage(mCurrentPage);
//    }
  }


  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    ButterKnife.bind(this, view);
    prepareUI();
//    getNewsByPage(mCurrentPage);
  }

  private void prepareUI() {
    mNews = new ArrayList<News>();
//    mLayoutManager = new GridLayoutManager(mContext, 2);
    mLayoutManager = new LinearLayoutManager(mContext);
    mRecyclerView.setLayoutManager(mLayoutManager);
    mEndlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(mLayoutManager) {
      @Override
      public void onLoadMore(int page, int totalItemsCount) {
//          L.d("Page : " + page  );
//          // Nghi vấn bug load more
////          if (page - mCurrentPage == 1 || page - mCurrentPage == 0){
////          if (totalItemsCount <= page * 10) {
//            mCurrentPage = page + 1;
////            if (mNews.size() != 0)
//              getNewsByPage(page + 1);
////          }
////          }
        L.d("onLoadMore() : " + mCurrentPage);
        if (mNews.size() != 0) {
          mProgressBarLoadmore.setVisibility(View.VISIBLE);
          getNewsByPage(mCurrentPage);
        }
      }
    };
    mRecyclerView.addOnScrollListener(mEndlessRecyclerViewScrollListener);
    mSwipeRefreshLayout.setRefreshing(true);
    mIsFirstLaunch = true;

    mSwipeRefreshLayout.setOnRefreshListener(this);
  }

  public void getNewsByPage(int page) {
//    L.d("GetNewByPage() : " + page);
    if (NetworkHelper.isNetworkAvailable(mContext))
      new GetNewsAsynctask(this).execute(UrlHelper.getUrlByPage(mUrl, page));
    else {
      loadDataToView(null);
    }
  }

  @Override
  public void onSuccess(ArrayList<News> news) {
    loadDataToView(news);
  }

  private void loadDataToView(ArrayList<News> news) {
//    L.d("Url : " + UrlHelper.getUrlByPage(mUrl, mCurrentPage));
    // no data
    boolean isNodata = false;
    if (news == null || news.size() == 0) {
      isNodata = true;
//      showViewError("Network is error");
    }
    // have
    else {
      if (mCurrentPage == 1) {
        mNews.clear();
      }
      mNews.addAll(news);
      mCurrentPage++;
    }

    if (mIsFirstLaunch) {
      L.d("is first launch");
      mNewsAdapter = new NewsAdapter(mContext, mNews);
      mNewsAdapter.setOnClickListener(this);
      mRecyclerView.setAdapter(mNewsAdapter);


      mIsFirstLaunch = false;
    }

    // config to linear layout for recyclerview
//    if (mNews.size() == 0 && isNodata) {
//      mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
//    }

    mNewsAdapter.notifyDataSetChanged();
//    if (news.size() > 0) {
//    if (mNews.size() > 0) {
//      mLayoutManager.scrollToPositionWithOffset(7, 0);
//    }
    mSwipeRefreshLayout.setRefreshing(false);
    mProgressBarLoadmore.setVisibility(View.INVISIBLE);
  }

  @Override
  public void onFailure(String error) {
    loadDataToView(null);
    showViewError(error);
  }

  @Override
  public void onNodata() {
//    Toast.makeText(mContext, "On nodata", Toast.LENGTH_SHORT).show();
    mSwipeRefreshLayout.setRefreshing(false);
    mProgressBarLoadmore.setVisibility(View.INVISIBLE);
  }

  private void showViewError(String error) {
    Toast.makeText(mContext, error, Toast.LENGTH_SHORT).show();
    mSwipeRefreshLayout.setRefreshing(false);
    mProgressBarLoadmore.setVisibility(View.INVISIBLE);
  }

  @Override
  public void onClick(int position) {
    News news = mNews.get(position);
    Intent i = new Intent(mContext, DetailNewsActivity.class);
//    i.putExtra(Const.KEY_NEWS_URL, news.link);
    Bundle bundle = new Bundle();
    bundle.putSerializable("content", news);
    i.putExtra("bundle", bundle);
    startActivity(i);
  }

  @Override
  public void onRefresh() {
    mCurrentPage = 1;
    mSwipeRefreshLayout.setRefreshing(true);

    mEndlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(mLayoutManager) {
      @Override
      public void onLoadMore(int page, int totalItemsCount) {
        L.d("onLoadMore() : " + mCurrentPage);
        if (mNews.size() != 0) {
          mProgressBarLoadmore.setVisibility(View.VISIBLE);
          getNewsByPage(mCurrentPage);
        }
      }
    };
    mRecyclerView.addOnScrollListener(mEndlessRecyclerViewScrollListener);
    getNewsByPage(mCurrentPage);
  }
}
