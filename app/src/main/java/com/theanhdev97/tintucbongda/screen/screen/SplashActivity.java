package com.theanhdev97.tintucbongda.screen.screen;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.theanhdev97.tintucbongda.R;

public class SplashActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_sflash);
    new Handler().postDelayed(new Runnable() {
      @Override
      public void run() {
//        startActivity(new Intent(SplashActivity.this, HomeActivity.class));
        startActivity(new Intent(SplashActivity.this, Home2Activity.class));
        finish();
      }
    }, 4000);
    getWindow().getAttributes().windowAnimations = R.style.fade;
  }

}
