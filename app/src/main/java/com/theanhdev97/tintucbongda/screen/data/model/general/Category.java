package com.theanhdev97.tintucbongda.screen.data.model.general;

/**
 * Created by DELL on 21/05/2018.
 */

public class Category {
  public String title;
  public Integer image;

  public Category(String title, Integer image) {
    this.title = title;
    this.image = image;
  }
}
